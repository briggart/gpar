module cls_mod
  
  use healpix_types ,only : dp ,i4b ,hpx_dbadval
  use lpl_utils_mod ,only : myTT ,myEE ,myBB ,myTE ,myTB ,myEB 
  implicit none

  private
  public aps ,myTT ,myEE ,myBB ,myTE ,myTB ,myEB 
  
  type :: aps

     real(dp) ,allocatable :: cl(:,:)
     integer(i4b)          :: lmin ,lmax 

!     real(dp) ,pointer ,dimension(:) :: TT =>null() ,EE =>null() ,BB =>null()&
!          ,TE =>null() ,TB =>null() ,EB =>null() 

   contains

     procedure :: init   => init_aps
     procedure :: matrix => aps_matrix
     procedure :: cl2ascii 
     procedure :: clean => clean_aps
     final     :: final_aps
     
  end type aps

contains

  subroutine final_aps(self)
    type(aps)  :: self

    call self%clean()
  end subroutine final_aps
  
  subroutine init_aps(self ,lmin ,lmax)
    class(aps) ,intent(inout) :: self
    integer(i4b)  ,intent(in) :: lmin ,lmax

    call self%clean()
    self%lmin = lmin
    self%lmax = lmax

    allocate(self%cl(lmin:lmax,6) ,source = 0._dp)

!    self%TT(lmin:lmax) => self%cl(lmin:lmax,1)
!    self%EE(lmin:lmax) => self%cl(lmin:lmax,2)
!    self%BB(lmin:lmax) => self%cl(lmin:lmax,3)
!    self%TE(lmin:lmax) => self%cl(lmin:lmax,4)
!    self%TB(lmin:lmax) => self%cl(lmin:lmax,5)
!    self%EB(lmin:lmax) => self%cl(lmin:lmax,6)

  end subroutine init_aps

  subroutine clean_aps(self)
    class(aps) ,intent(inout) :: self
    integer :: status

!    self%TT => null()
!    self%EE => null()
!    self%BB => null()
!    self%TE => null()
!    self%TB => null()
!    self%EB => null()

    if(allocated(self%cl)) deallocate(self%cl,stat=status)

    self%lmin = -1
    self%lmax = -1

  end subroutine clean_aps

  function aps_matrix(self,l,bl) result(cov)
    class(aps)   ,intent(in) :: self
    integer(i4b) ,intent(in) :: l
    real(dp)     ,intent(in) ,optional :: bl(3)

    real(dp) :: cov(3,3) ,fl(3)

    if(l .lt. self%lmin .or. l .gt. self%lmax) then
       cov = hpx_dbadval
       return
    end if

    if(present(bl)) then 
       fl = bl
    else
       fl = 1
    end if

    cov(1,1) = self%cl(l,myTT)*fl(1)*fl(1)
    cov(2,1) = self%cl(l,myTE)*fl(2)*fl(1)
    cov(3,1) = self%cl(l,myTB)*fl(3)*fl(1)

    cov(2,2) = self%cl(l,myEE)*fl(2)*fl(2)
    cov(3,2) = self%cl(l,myEB)*fl(3)*fl(2)

    cov(3,3) = self%cl(l,myBB)*fl(3)*fl(3)

    cov(1,2) = cov(2,1)
    cov(1,3) = cov(3,1)
    cov(2,3) = cov(3,2)
    
  end function aps_matrix

  subroutine cl2ascii(self,outfile,lmin,lmax)
    class(aps)       ,intent(in)       :: self
    character(len=*) ,intent(in)       :: outfile
    integer(i4b) ,intent(in) ,optional :: lmin ,lmax

    integer(i4b) :: l ,linf ,lsup ,unit

    if (present(lmin)) then
       linf = max(lmin,self%lmin)
    else
       linf = self%lmin
    end if

    if (present(lmax)) then
       lsup = min(lmin,self%lmin)
    else
       lsup = self%lmax
    end if

    open(newunit=unit,file=outfile,status='unknown',action='write')
    do l = linf ,lsup
       write(unit,'(1I6,6E16.7)') l,self%cl(l,:)
    end do
    close(unit)

  end subroutine cl2ascii

  subroutine decompose_3x3(cov,evec,eval)
    real(dp) ,intent(inout) :: cov(3,3)
    real(dp) ,intent(out)   :: evec(3,3) ,eval(3)

    integer ,parameter :: lwork = 99 ,liwork = 30 !for optimal performance 
    integer            :: isuppz(6) ,iwork(liwork) ,neigen ,info
    real(8)            :: work(lwork) ,dlamch
    external dlamch

    call dsyevr('V','A','L',3,cov,3,0.d0,0.d0,0,0,dlamch('Safe minimum')&
         ,neigen,eval,evec,3,isuppz,work,lwork,iwork,liwork,info)

  end subroutine 

  subroutine invert_3x3(evec,eval,cov)
    !return inverse of 3x3 matrix given its eigenvalue decomposition
    real(dp) ,intent(in)  :: evec(3,3) ,eval(3)
    real(dp) ,intent(out) :: cov(3,3)

    real(dp) ,parameter   :: lim = 1.d-13
    real(dp)              :: tmp(3,3)
    integer(i4b)          :: i

    do i = 1,3
       if (eval(i) .gt. lim*eval(3)) then
          tmp(:,i) = evec(:,i)/sqrt(eval(i))
       else
          tmp(:,i) = 0._dp
       end if
    end do

    cov = 0._dp
    call dsyrk('L','N',3,3,1.d0,tmp,3,0.d0,cov,3)  
    
  end subroutine invert_3x3
  
end module cls_mod
