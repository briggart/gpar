module chain_mod

  use healpix_types      ,only : dp ,sp ,i4b ,i8b ,twopi
  use healpix_modules    ,only : output_map ,write_minimal_header ,nside2npix
  use CAMB_interface_mod ,only : get_cls
  use rng_mod            ,only : rand_g ,rand_u
  use lpl_utils_mod      ,only : unmask_vec
  use cls_mod
  use cosmo_params_mod
  use real_harmonics_mod
  use gpar_data_mod

  implicit none

  private
  public chain_sample
  
  type :: chain_sample

     private
     type(real_alm)     :: s, f
     type(aps)          :: cl
     type(cosmo_params) :: theta

     real(dp) ,allocatable :: mean(:,:) ,delta(:,:) ,invSpN(:,:)
     real(dp) ,allocatable :: invS(:,:,:) ,sqrtS(:,:,:) ,invSqrtS(:,:,:) 
     real(dp)              :: pi_f ,pi
     integer(i4b)          :: chain_id ,sample_id ,mult

   contains

     procedure :: init => init_chain_sample
     procedure :: new_sample => gen_new_sample
     procedure :: to_file => output_chain_sample

  end type chain_sample

  type(chain_sample) ,save :: test
  
contains

  subroutine init_chain_sample(self,chain_id,theta,lmin,lmax,sizeS,nside,s,f)
    class(chain_sample) ,intent(out) :: self
    integer(i4b)        ,intent(in)  :: chain_id ,lmin ,lmax ,nside
    integer(i8b)        ,intent(in)  :: sizeS
    class(cosmo_params) ,intent(in)  :: theta
    class(real_alm) ,optional ,intent(in)  :: s ,f

    real(dp)     :: lcov(3,3) ,inv(3,3) ,invsqrt(3,3) ,sqrtS(3,3)
    integer(i4b) :: l ,m ,id
    integer(i8b) :: npix

    self%pi        = -1.d100
    self%chain_id  = chain_id
    self%sample_id = 0
    self%mult      = 0
    call self%cl%init(lmin,lmax)

    self%theta = theta
    call get_cls(self%theta,self%cl)

    if (present(s)) then
       self%s = s
    else
       call self%s%init(lmin,lmax)
       self%s%alm = hpx_dbadval
    end if
    if (present(f)) then
       self%f = f
    else
       call self%f%init(lmin,lmax)
       self%f%alm = 0
    end if
    
    npix = 12_i8b*nside*nside
    allocate(self%mean(0:npix-1,3) ,self%delta(0:npix-1,3) &
         ,source = hpx_dbadval)

    allocate(self%invSpN(sizeS,sizeS) ,source = 0._dp)
    allocate(self%invS(3,3,lmin:lmax) ,self%invSqrtS(3,3,lmin:lmax) &
         ,self%sqrtS(3,3,lmin:lmax) ,source = 0._dp)
    
    do l = lmin ,lmax
       lcov = self%cl%matrix(l)*twopi/(l*(l+1))
       call invert_3x3(lcov ,sqrtS ,inv,invsqrt)

       self%sqrtS(:,:,l) = sqrtS
       self%invS(:,:,l) = inv
       self%invsqrtS(:,:,l) = invsqrt

    end do

  end subroutine init_chain_sample

  subroutine invert_3x3(cov,sqrtS,inv,invsqrt)
    !assume TB = EB  =0
    !should be faster than general dsyevr 
    real(dp) ,intent(in)  :: cov(3,3)
    real(dp) ,intent(out) :: sqrtS(3,3)
    real(dp) ,intent(out) :: inv(3,3)
    real(dp) ,intent(out) :: invsqrt(3,3)
    
    real(dp) :: e1 ,e2 ,R(2,2) ,phi ,delta ,S(2,2) ,d11_22 ,s11_22
    
    sqrtS   = 0._dp
    inv     = 0._dp
    invsqrt = 0._dp
    
    sqrtS(3,3)   = sqrt(cov(3,3))
    inv(3,3)     = 1._dp/cov(3,3)
    invsqrt(3,3) = sqrt(1._dp/cov(3,3))
    
    ! M = | A  B | = R E R^t
    !     | B  C |
    !
    ! E = diag(e1 ,e2)
    ! e1 = (A +C +D)/2
    ! e2 = (A +C -D)/2
    ! D = sign(A-C)*sqrt(((A-C)**2 +4B**2))
    !
    ! R = | cos(phi) -sin(phi) |
    !     | sin(phi)  cos(phi) |
    ! phi = 0.5*atan(2B/(A-C))

    d11_22 = cov(1,1) -cov(2,2)
    s11_22 = cov(1,1) +cov(2,2)

    delta = sign(1._dp ,d11_22)*sqrt(d11_22**2 +4*cov(2,1)**2)

    !directly compute 4th root of eigenvalues
!    e1 = sqrt((s11_22 +delta)/2)
!    e2 = sqrt((s11_22 +cov(2,2) -delta)/2)
    e1 = ((s11_22 +delta)/2)**0.25_dp
    e2 = ((s11_22 -delta)/2)**0.25_dp
    
    phi = atan(2*cov(2,1)/d11_22)/2
    R(1,1) =  cos(phi)
    R(2,1) =  sin(phi)
    R(1,2) = -sin(phi)
    R(2,2) =  cos(phi)
    
    S(:,1) = R(:,1)*e1
    S(:,2) = R(:,2)*e2
!    S(:,1) = R(:,1)*sqrt(e1)
!    S(:,2) = R(:,2)*sqrt(e2)
    sqrtS(1:2,1:2) = recompose_2x2(S)

!    R(:,1) = R(:,1)/sqrt(e1)
!    R(:,2) = R(:,2)/sqrt(e2)
    R(:,1) = R(:,1)/e1
    R(:,2) = R(:,2)/e2
    invsqrt(1:2,1:2) = recompose_2x2(R)
    
!    R(:,1) = R(:,1)/sqrt(e1)
!    R(:,2) = R(:,2)/sqrt(e2)
    R(:,1) = R(:,1)/e1
    R(:,2) = R(:,2)/e2
    inv(1:2,1:2) = recompose_2x2(R)

  contains

    function recompose_2x2(A) result(B)
      real(dp) ,intent(in) :: A(2,2)
      real(dp)             :: B(2,2)
      real(dp)             :: AT(2,2)
      
      AT = transpose(A)

      B(1,1) = sum(AT(:,1)*AT(:,1))
      B(2,1) = sum(AT(:,2)*AT(:,1))
      B(1,2) = B(2,1)
      B(2,2) = sum(AT(:,2)*AT(:,2))
!      B(1,1) = sum(A(1,:)*AT(:,1))
!      B(2,1) = sum(A(2,:)*AT(:,1))
!      B(1,2) = B(2,1)
!      B(2,2) = sum(A(2,:)*AT(:,2))
      
    end function recompose_2x2

  end subroutine invert_3x3

  subroutine gen_new_sample(self,D)

    class(chain_sample) ,intent(inout) :: self
    class(gpar_data)    ,intent(inout) :: D 

    integer ,parameter :: ntry = 100
    
    real(dp) :: lcov(3,3) ,inv(3,3) ,invsqrt(3,3) ,sqrtS(3,3) ,R ,z 
    real(dp) ,allocatable :: tmp1(:,:) ,tmp2(:,:) ,vf(:) ,vs(:) 


    integer(i4b) :: nspectra ,i ,id ,l ,m ,info ,nm
    logical      :: accept ,ok
    
    accept = .false.
    associate(lmin => D%lmin ,lmax => D%lswitch ,y => D%y ,nall => D%y%nall ,ntot => d%y%ntot) 

      if(y%has_x) then
         nspectra = 3
      elseif(y%has_p) then
         nspectra = 2
      else
         nspectra = 1
      end if

      test = self

      !propose new parameters
      ok = .false.
      i = 0
      do while(.not. ok)
         call D%w%propose(self%theta,test%theta)
         
         !estimate cls 
         call get_cls(test%theta,test%cl,ok)
         i = i+1
         if(i .gt. ntry) stop 'could not generate parameters'
      end do
!      test%theta = self%theta
!      test%cl = self%cl


      !compute S^1/2, S^-1 and S^-1/2
      i  = 1
      do l = lmin ,lmax
         nm = 2*l+1
         lcov = test%cl%matrix(l)*twopi/(l*(l+1))
         call invert_3x3(lcov ,sqrtS ,inv ,invsqrt)
         test%sqrtS(:,:,l)    = sqrtS
         test%invS(:,:,l)     = inv
         test%invsqrtS(:,:,l) = invsqrt

         !filter fluctuation alms
         lcov = 0._dp
         invsqrt   = self%invsqrtS(:,:,l)
         !matrixes are symmetric, A(:,i) = A(i,:)
         lcov(1,1) = sum(sqrtS(1:2,1)*invsqrt(1:2,1))
         lcov(2,1) = sum(sqrtS(1:2,2)*invsqrt(1:2,1))
         lcov(1,2) = sum(sqrtS(1:2,1)*invsqrt(1:2,2))
         lcov(2,2) = sum(sqrtS(1:2,2)*invsqrt(1:2,2))
         lcov(3,3) = sqrtS(3,3)*invsqrt(3,3)

         tmp1 = transpose(self%f%alm(i:i+2*l,:)) 
         allocate(tmp2 ,mold = tmp1) 
         call dsymm('L','L',3,nm,1._dp,lcov,3,tmp1,3,0._dp,tmp2,3)
         test%f%alm(i:i+2*l,:) = transpose(tmp2) 
         deallocate(tmp2)
         i  = i  +nm

      end do
      deallocate(tmp1)

      !solve for new mean map
      ! s = (S^-1 + A N^-1 A)^-1  A N^-1 D
      id = 1
      test%invSpN = D%AinvNA
      do l = lmin,lmax
         do m = 1,2*l+1
            test%invSpN(id:id+2,id:id+2) = test%invSpN(id:id+2,id:id+2) &
                 +test%invS(:,:,l)
            id = id +3
         end do
      end do

      call dpotrf('L',nall,test%invSpN,nall,info)
      if(info .ne. 0) then
         stop 'matrix is not invertible in gen_new_sample'
      end if

      !new mean field map
      vs = D%AinvNd
      call dpotrs('L',nall,1,test%invSpN,nall,vs,nall,info)
      call test%s%vec2alm(vs,nspectra)

      !now we have everything we need to compute accept probability
      if (self%sample_id .eq. 0) then 
         !sample 0 is just finishing initialization, so just accept it and move on with your life
         test%pi   = hpx_dbadval
         test%pi_f = hpx_dbadval 
         accept = .true.
         deallocate(vs)

      else

         ! exp(-1/2 s S^-1 s)
         vf = vs
         id = 1
         do l = lmin ,lmax
            nm = 2*l+1
            tmp1 = reshape(vs(id:id +nm*nspectra-1),[3,nm]) 
            allocate(tmp2 ,mold = tmp1) 
            call dsymm('L','L',3,nm,1._dp,test%invS(:,:,l),3,tmp1,3,0._dp,tmp2,3)
            vf(id:id +nm*nspectra-1) = pack(tmp2,mask=.true.) 
            deallocate(tmp2)
            id  = id  +nm*nspectra
         end do

         test%pi = -.5_dp*sum(vf*vs)
         write(*,*) '========================================='
         write(*,*) '-1/2 s S^-1 s =', -.5_dp*sum(vf*vs)

         ! exp(-1/2 f N^-1 f)
         vf = test%f%project_onto(y)
         vs = vf
         call dsymv('L',ntot,1._dp,D%invN,ntot,vf,1,0.d0,vs,1)
         test%pi_f = -.5_dp*sum(vf*vs)
         write(*,*) '-1/2 f N^-1 f = ', test%pi_f

         ! exp(-1/2 (d -As) N^-1 (d -As)
         vs = test%s%project_onto(y)
         vf = D%d - vs
         call unmask_vec(y%lmask,vs)
         test%mean = reshape(vs,[nside2npix(D%nside),3_i8b])
         vs = vf
         call dsymv('L',ntot,1._dp,D%invN,ntot,vf,1,0.d0,vs,1)
         test%pi = test%pi -.5_dp*sum(vf*vs)
         write(*,*) '-1/2 (d -As) N^-1 (d -As) =',-.5_dp*sum(vf*vs)
         write(*,*) '========================================='
         deallocate(vf,vs)
         
         !compute acceptance probability
         R = min(1._dp, exp((test%pi +test%pi_f) - (self%pi +self%pi_f)))
         write(*,*) 'R = ',R ,test%pi +test%pi_f ,self%pi +self%pi_f
         z = rand_u()                           
         accept = z .le. R
      end if

      if(accept) then
         self%invS     = test%invS
         self%invSqrtS = test%invSqrtS
         self%invSpN   = test%invSpN
         self%sqrtS    = test%sqrtS
         self%mean  = test%mean
         self%theta = test%theta
         self%pi    = test%pi
         self%cl    = test%cl
         self%s     = test%s
         self%sample_id = self%sample_id +1
         self%mult = 1
      else
         self%sample_id = self%sample_id +1
         self%mult = self%mult +1
      end if

      !generate new fluctuation map

      !vf = S^-1/2 w_0
      allocate(vs(nall) ,vf(nall) ,source = 0.d0)
      do i = 1,nall
         vs(i) = rand_g()
      end do
      id = 1
      do l = lmin ,lmax
         nm = (2*l+1)
         tmp1 = reshape(vs(id:id +nm*nspectra -1),[3,nm]) 
         allocate(tmp2 ,mold = tmp1) 
         call dsymm('L','L',3,nm,1._dp,self%invSqrtS(:,:,l),3,tmp1,3,0._dp,tmp2,3)
         vf(id:id +nm*nspectra-1) = pack(tmp2,mask=.true.) 
         deallocate(tmp2)
         id  = id  +nm*nspectra
      end do

      !vf = S^-1/2 w_0 + A N^-1/2 w_1
      deallocate(vs)
      allocate(vs(ntot))
      do i = 1,ntot
         vs(i) = rand_g()
      end do
      call dgemv('N',nall,ntot,1.d0,D%AinvsqrtN,nall,vs,1,1.d0,vf,1)
!      call dgemv('N',nall,ntot,1.d0,D%AinvsqrtN,nall,vs,1,0.d0,vf,1)

      !vf = (S^-1 +A N^-1 A)^-1 ( S^-1/2 w_0 + A N^-1/2 w_1)
      call dpotrs('L',nall,1,self%invSpN,nall,vf,nall,info)

      call self%f%vec2alm(vf,nspectra)
      vf = self%f%project_onto(y)

      vs = vf
      call dsymv('L',ntot,1._dp,D%invN,ntot,vf,1,0.d0,vs,1)
      self%pi_f = -.5_dp*sum(vf*vs)
      call unmask_vec(y%lmask,vf)
      self%delta = reshape(vf,[nside2npix(D%nside),3_i8b])
            
      deallocate(vs ,vf)
      
    end associate

    !dump to file
    call self%to_file(D%oroot,D%y%s)

  end subroutine gen_new_sample
    
  
  subroutine output_chain_sample(self,root,s)
    class(chain_sample)        ,intent(in) :: self
    character(len=*) ,optional ,intent(in) :: root
    type(map_stats)  ,optional ,intent(in) :: s

    integer(i4b) ,parameter :: nlhdr = 120
    integer(i4b)            :: unit
    type(aps)               :: sigma

    character(len=:) ,allocatable :: r
    character(len=2)   :: cstring
    character(len=5)   :: sstring
    character(len=512) :: ofile
    character(len=30)  :: fm
    character(len=80)  :: hdr(nlhdr)
    
    if(present(root)) then
       allocate(r,source=root)
    else
       allocate(r,source='')
    end if
       
    write(cstring,'(i2.2)') self%chain_id
    write(sstring,'(i5.5)') self%sample_id

    ofile = trim(r)//'cl_c'//cstring//'_s'//sstring//'.txt'
    call self%cl%cl2ascii(trim(ofile))

    sigma = self%s%alm2cl()
    ofile = trim(r)//'sl_c'//cstring//'_s'//sstring//'.txt'
    call sigma%cl2ascii(trim(ofile))

    sigma = self%f%alm2cl()
    ofile = trim(r)//'fl_c'//cstring//'_s'//sstring//'.txt'
    call sigma%cl2ascii(trim(ofile))
    
    write(fm,'(a,i2,a)')"(2x,i4,2x,e18.12,",ncp,"(2x,e13.6))"
    ofile = trim(r)//'params_c'//cstring//'_s'//sstring//'.txt'
    open(newunit=unit,file=trim(ofile),status='replace',action='write')
    write(unit,fm) self%mult,self%pi+self%pi_f,self%theta%par2vec()
    close(unit)

    if (present(s)) then
       call write_minimal_header(hdr,'map',order=s%ordering,nside=s%nside)
       ofile = '!'//trim(r)//'cmb_c'//cstring//'_s'//sstring//'.fits'
       call output_map(self%mean,hdr,ofile)
       ofile = '!'//trim(r)//'fluct_c'//cstring//'_s'//sstring//'.fits'
       call output_map(self%delta,hdr,ofile)
    end if
    
    deallocate(r)
    
  end subroutine output_chain_sample

end module chain_mod
