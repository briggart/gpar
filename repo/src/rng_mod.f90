module rng_mod
  !using healpix rng for now

  use healpix_types ,only : dp
  use rngmod
  implicit none

  type(planck_rng) ,save :: rngh
  private

  public init_rng ,rand_u ,rand_g
  
contains

  subroutine init_rng(s1,s2,s3,s4)

    integer            ,intent(in)    :: s1
    integer ,optional  ,intent(in)    :: s2 ,s3 ,s4

    integer :: i2 ,i3 ,i4

    i2 = 0 ;i3 = 0 ;i4 = 0
    if(present(s2)) i2 = s2
    if(present(s3)) i3 = s3
    if(present(s4)) i4 = s4

    call rand_init(rngh,s1,i2,i3,i4)

  end subroutine init_rng

  function rand_u() result (r)
    real(dp) :: r

    r = rand_uni(rngh)
    
  end function rand_u

  function rand_g() result (r)
    real(dp) :: r

    r = rand_gauss(rngh)
    
  end function rand_g

end module rng_mod
