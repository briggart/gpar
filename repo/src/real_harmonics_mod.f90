module real_harmonics_mod

  use healpix_types   ,only : i4b ,i8b ,dp ,pi
  use healpix_modules ,only : plm_gen ,pix2ang_ring ,in_ring ,hpx_dbadval
  use cls_mod
  use lpl_utils_mod
  implicit none

  private
  public real_alm ,real_ylm ,get_nmodes

  type :: real_alm

     integer(i4b)          :: lmin ,lmax
     integer(i8b)          :: nlm 
     real(dp) ,allocatable :: alm(:,:)

   contains

     procedure :: init    => init_alm
     procedure :: filter  => filter_alm 
     procedure :: project_onto
     procedure :: alm2cl
     procedure :: vec2alm
     
  end type real_alm


  type ,extends(sky_mask)  :: real_ylm
     
     logical               :: has_t ,has_p ,has_x
     integer(i4b)          :: lmin  ,lmax ,npol
     integer(i8b)          :: nlm  ,nall
     !T,E,B storing order:
     !
     ! Y_l0,T; Y_l0,E; Y_l0,B; Y_l-1,T; ...; Y_l-1,B; Y_l1,T; ...; Y_ll,B; Y_l+10,T; ...
     !
     !analogous pattern for T-only and EB-only 
     real(dp) ,allocatable :: ylm(:,:) 
     
   contains

     procedure :: init  => compute_modes
!     procedure :: reorder => reorder_pol2emm
!     final     :: clean_plms
     
  end type real_ylm
     
contains

  subroutine init_alm(self,lmin,lmax)
    class(real_alm) ,intent(inout) :: self
    integer(i4b)    ,intent(in)    :: lmin ,lmax

    self%lmin = lmin
    self%lmax = lmax
    self%nlm  = get_nmodes(lmin,lmax)

    allocate(self%alm(self%nlm,3) ,source = 0._dp)
    
  end subroutine init_alm
  
  integer(i8b) function get_nmodes(lmin,lmax) result(nlm)
    integer(i4b) ,intent(in) :: lmin ,lmax

    nlm = (lmax+1)**2 - lmin**2
    
  end function get_nmodes
  
  subroutine filter_alm(self,bl)
    class(real_alm) ,intent(inout) :: self
    real(dp)        ,intent(in)    :: bl(:,:)

    self%alm(self%lmin:self%lmax,1:3) = self%alm(self%lmin:self%lmax,1:3)*&
         bl(self%lmin:self%lmax,1:3)
    
    
  end subroutine filter_alm

  function alm2cl(self) result(cl)
    class(real_alm) ,intent(in) :: self
    type(aps)                   :: cl

    integer(i4b)                :: l ,nl ,id
    
    call cl%init(self%lmin,self%lmax)

    id = 1
    do l = self%lmin ,self%lmax
       nl = 2*l+1
       cl%cl(l,myTT) = sum(self%alm(id:id+nl-1,1)*self%alm(id:id+nl-1,1))/nl
       cl%cl(l,myTE) = sum(self%alm(id:id+nl-1,1)*self%alm(id:id+nl-1,2))/nl
       cl%cl(l,myTB) = sum(self%alm(id:id+nl-1,1)*self%alm(id:id+nl-1,3))/nl
       cl%cl(l,myEE) = sum(self%alm(id:id+nl-1,2)*self%alm(id:id+nl-1,2))/nl
       cl%cl(l,myEB) = sum(self%alm(id:id+nl-1,2)*self%alm(id:id+nl-1,3))/nl
       cl%cl(l,myBB) = sum(self%alm(id:id+nl-1,3)*self%alm(id:id+nl-1,3))/nl
       id = id+nl
    end do
    
  end function alm2cl

  subroutine reorder_pol2emm(self)
    class(real_ylm) ,intent(inout) :: self
    real(dp)        ,allocatable   :: p(:,:)
    integer(i4b)                   :: i
    
    if(.not. self%has_p) then
       !all orderings are equivalent if there is no polarization
       allocate(p ,source = self%ylm)
       return
    else
       allocate(p ,mold = self%ylm)
    end if

    do i = 1,self%nall
       p(:,get_indx(i)) = self%ylm(:,i)
    end do
    self%ylm = p
    deallocate(p)
    
  contains

    integer function get_indx(i) result(indx)
      integer ,intent(in) :: i
      integer             :: ell ,emm ,off ,j
      
      !first let's find out if it's a T, E or B mode
      off = (i-1)/(self%nlm)
      j   = i - off*(self%nlm) 
      indx = (j-1)*self%npol +1 +off
      
    end function get_indx
    
  end subroutine reorder_pol2emm
  
  subroutine compute_modes(self,maskfile,lmin,lmax,wl)
    implicit none
    !compute legendre (associated) functions and mask them

    class(real_ylm) ,intent(inout) :: self
    character(len=*),intent(in)    :: maskfile
    integer(i4b)    ,intent(in)    :: lmin ,lmax
    real(dp)        ,intent(in) ,optional :: wl(0:,:)  
    
    real(dp) ,allocatable :: plms(:,:) ,maps(:,:,:,:) ,theta(:) ,phi(:) &
         ,y(:,:) ,v(:) ,xl(:,:)
    real(dp)              :: xnorm 
    
    integer(i4b) :: nsmax ,nlmax ,nmmax ,n_plm ,npol ,nrings ,nchunk ,nl0 &
         ,i ,id ,ipol ,l ,istart ,istop ,ntheta ,m ,switch ,off
    integer(i4b) ,allocatable :: list(:,:) ,nri(:)

    
    if (present(wl)) then
       allocate(xl ,source = wl)
    else
       allocate(xl(0:lmax,3) ,source = 1._dp)
    end if


    call self%set_mask_from_file(maskfile)
    
    self%lmax  = lmax
    self%lmin  = lmin
!    self%ntot  = 
    self%has_t = self%ntemp .gt. 0
    self%has_p = self%nqu   .gt. 0
    self%has_x = self%has_t .and. self%has_p
    self%npol  = 0
    off = 0
    if(self%has_t) then
       self%npol = self%npol +1
       off = 1
    end if
    if(self%has_p) then
       self%npol = self%npol +2
       npol = 3
    else
       npol = 1
    end if
    self%nlm  = get_nmodes(lmin,lmax)
    self%nall = self%nlm *self%npol

!    write(0,*) '>>>>>>>>>>>>>>>',self%nlm,self%nall,'<<<<<<<<<<<<<<<<<'
    
    nsmax = self%s%nside
    nlmax = lmax
    nmmax = lmax
    n_plm = nsmax*(nmmax +1)*(2*nlmax -nmmax+2)
!    nchunk = nlmax +1 +(nlmax*(nlmax+1))/2
    nchunk = ((nlmax +1)*(nlmax +2))/2
    

    allocate(plms(0:n_plm-1,1:npol) ,source = 0._dp)
    call plm_gen( nsmax, nlmax, nmmax, plms)

    !now map them to the sphere...
    
    allocate(theta(0:self%s%npix-1) ,phi(0:self%s%npix-1))
    
    do i = 0,self%s%npix -1 
       call pix2ang_ring(nsmax, i, theta(i), phi(i))
    end do

    nrings = 4*nsmax - 1
    allocate(list(0:nrings,nrings))
    allocate(nri(nrings))
    do i = 1,nrings
       call in_ring(nsmax,i,0._dp,pi,list(:,i),nri(i))
    end do

    allocate(maps(0:self%s%npix-1,0:nlmax,0:nmmax,1:npol) ,source = 0._dp)
    allocate(v(nchunk) ,source = 0._dp)

    ntheta = 2*nsmax 
    do ipol = 1,npol
       istart = 0
       if(ipol .eq. 3) then
          switch = -1
       else
          switch = 1
       end if
       do i = 1,ntheta
          istop = istart +(nchunk-1)
          v(1:nchunk) = plms(istart:istop,ipol)
          id    = 0
          do m = 0,nmmax
             do l = m,nlmax
                id = id +1
                maps(list(0:nri(i)-1,i)                  ,l,m,ipol) = v(id)
                maps(list(0:nri(nrings+1-i)-1,nrings+1-i),l,m,ipol) = v(id)*switch*(-1)**(l+m)
             end do
          end do
          istart = istop+1
       end do
    end do
    deallocate(plms,v,nri,list)

    allocate(y(0:self%s%npix-1,npol)  ,source = 0._dp)
    allocate(plms(self%ntot,self%nall) ,source = 0._dp)
    id = 0
    if(self%has_t) then
       
       do l = lmin,nlmax
          !for each l we store modes in order of m = 0,-1,+1,-2,+2,...,-l,+l
          id = id +1
!          xnorm = clnorm(l,myTT)*fourpi/(2*l+1_dp)

          !m = 0
          y      = 0._dp
          y(:,1) = maps(:,l,0,1)
          call reorder_map(y,from=1,to=self%s%ordering)
          v      = map2vec(y)
          call mask_vec(self%lmask,v)
          plms(:,id) = v*xl(l,1)
          xnorm = sum(plms(:,id)**2)
          
          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,1) = sqrt(2._dp)*maps(:,l,m,1)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,1)
             xnorm = xnorm + sum(plms(:,id)**2)

             !positive m
             id     = id+1
             y      = 0._dp
             y(:,1) = sqrt(2._dp)*maps(:,l,m,1)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,1)
             xnorm = xnorm +sum(plms(:,id)**2)
          end do
          xnorm = xnorm/(2*l+1)
          do m = id-2*l,id
             plms(:,m) = plms(:,m)*sqrt(xnorm/sum(plms(:,m)**2)) 
          end do
       end do
    end if

    if (self%has_p) then
       !EE
       do l = lmin,lmax
          nl0 = 2*l+1 
!          id  = off*nl0+1
          id  = id +1
          !m = 0
          y      = 0._dp
          y(:,2) =  -maps(:,l,0,2)
          y(:,3) =   maps(:,l,0,3)
          call reorder_map(y,from=1,to=self%s%ordering)
          v      = map2vec(y)
          call mask_vec(self%lmask,v)
          plms(:,id) = v*xl(l,2)
          xnorm = sum(plms(:,id)**2)
          
          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,2) =  -sqrt(2._dp)*maps(:,l,m,2)*sin(m*phi)*(-1)**m
             y(:,3) =   sqrt(2._dp)*maps(:,l,m,3)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,2)
             xnorm = xnorm + sum(plms(:,id)**2)
             
             !positive m
             id     = id+1
             y      = 0._dp
             y(:,2) =  -sqrt(2._dp)*maps(:,l,m,2)*cos(m*phi)*(-1)**m
             y(:,3) =  -sqrt(2._dp)*maps(:,l,m,3)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,2)
             xnorm = xnorm + sum(plms(:,id)**2)
          end do
          xnorm = xnorm/(2*l+1)
          do m = id-2*l,id
             plms(:,m) = plms(:,m)*sqrt(xnorm/sum(plms(:,m)**2)) 
          end do
       end do
       
     !BB
       do l = lmin,lmax
          nl0 = 2*l+1
          id = id+1
!          id  = (off+1)*nl0+1
!          xnorm = clnorm(l,myBB)*fourpi/(2*l+1_dp)
          !m = 0
          y      = 0._dp
          y(:,2) = -maps(:,l,0,3)
          y(:,3) = -maps(:,l,0,2)
          call reorder_map(y,from=1,to=self%s%ordering)
          v      = map2vec(y)
          call mask_vec(self%lmask,v)
          plms(:,id) = v*xl(l,3)
          xnorm = sum(plms(:,id)**2)

          do m = 1,l
             !negative m
             id     = id+1
             y      = 0._dp
             y(:,2) = -sqrt(2._dp)*maps(:,l,m,3)*cos(m*phi)*(-1)**m
             y(:,3) = -sqrt(2._dp)*maps(:,l,m,2)*sin(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,3)
             xnorm = xnorm + sum(plms(:,id)**2)

             !positive m
             id     = id+1
             y      = 0._dp
             y(:,2) =  sqrt(2._dp)*maps(:,l,m,3)*sin(m*phi)*(-1)**m
             y(:,3) = -sqrt(2._dp)*maps(:,l,m,2)*cos(m*phi)*(-1)**m
             call reorder_map(y,from=1,to=self%s%ordering)
             v      = map2vec(y)
             call mask_vec(self%lmask,v)
             plms(:,id) = v*xl(l,3)
             xnorm = xnorm + sum(plms(:,id)**2)
          end do
          xnorm = xnorm/(2*l+1)
          do m = id-2*l,id
             plms(:,m) = plms(:,m)*sqrt(xnorm/sum(plms(:,m)**2)) 
          end do
       end do
    end if

    deallocate(maps)
    call move_alloc(from=plms,to=self%ylm)
    call reorder_pol2emm(self)
    
  end subroutine compute_modes

  function project_onto(alm,ylm) result(v)
    class(real_alm) ,intent(in) :: alm
    class(real_ylm) ,intent(in) :: ylm
    real(dp)       ,allocatable :: v(:)
    real(dp)       ,allocatable :: a(:)

    associate (nlm => ylm%nlm ,ntot => ylm%ntot ,nall => ylm%nall)
      allocate(v(ntot))
      if (ylm%has_x) then
         !a = (a_l0,T;a_l0,E;a_l0,B;a_l-1,T,...,)
         a = pack(transpose(alm%alm),mask=.true.)
      else if(ylm%has_t) then
         a = alm%alm(:,1)
      else !ylm%has_p = .true.
         a = pack(transpose(alm%alm(:,2:3)),mask=.true.)
      end if
!      write(0,*) shape(ylm%ylm)
!      write(0,*) lbound(ylm%ylm(:,1)),ubound(ylm%ylm(:,1))
!      do i = 1,6
!         write(0,*) i,a(i),sum(ylm%ylm(1:3072,i)**2)
!      end do
      
      call dgemv('N',ntot,nall,1.d0,ylm%ylm,ntot,a,1,0.d0,v,1)
    end associate
    deallocate(a)
  end function project_onto

  subroutine vec2alm(self,v,nspectra)
    class(real_alm)   ,intent(inout) :: self
    real(dp)          ,intent(in)    :: v(:)
    integer ,optional ,intent(in)    :: nspectra

    integer :: ns ,nlm

    if(present(nspectra)) then
       ns = nspectra
    else
       !default is T,E,B
       ns = 3
    end if

    nlm = size(v)/ns
    if(nlm .ne. self%nlm) then
       self%alm = hpx_dbadval
    else
       select case(ns)
       case(1)
          self%alm(:,1) = v
       case(2,3)
          self%alm(:,4-ns:3) = transpose(reshape(v,[ns,nlm]))
       end select
    end if
       
  end subroutine vec2alm
  
end module real_harmonics_mod
