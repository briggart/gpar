program test_interface

  use cls_mod
  use cosmo_params_mod
  use CAMB_interface_mod
  implicit none

  integer ,parameter :: lmin = 2 ,lmax = 1500
  type(aps)          :: cl
  type(cosmo_params) :: P

  call cl%init(lmin=lmin,lmax=lmax)
  
  call init_camb()
  write(0,*) 'init done'
  
  call P%set(ombh2 = 0.0226d0 ,omch2 = 0.112d0 ,h0 = 70.d0 ,tau = 0.09d0 &
       ,ns = 0.96d0 ,As = 2.1d-9 ,r = 1.d0)

  call get_cls(P,cl)
  write(0,*) '1 done'
  call cl%cl2ascii('test_cls_tau_0.09.dat')
  
  P%tau   = 0.06d0
  call get_cls(P,cl)
  write(0,*) '2 done'
  call cl%cl2ascii('test_cls_tau_0.06.dat')


end program test_interface
