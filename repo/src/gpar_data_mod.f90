module gpar_data_mod

  use healpix_types   ,only : i4b ,i8b ,dp ,filenamelen ,hpx_dbadval
  use healpix_modules ,only : nside2npix ,pixel_window ,fits2cl
  use paramfile_io    ,only : paramfile_handle, parse_init, parse_real, &
       parse_double, parse_int, parse_long, parse_lgt, parse_string,    &
       parse_summarize, parse_finish
  use lpl_utils_mod   ,only : maxlen ,map2vec ,map_stats ,unmask_vec    &
       ,read_map ,mask_vec 
  use real_harmonics_mod
  use cosmo_params_mod
  use rng_mod
  implicit none
  
  type :: gpar_data

     integer(i4b)          :: ndata ,nside ,lmin ,lswitch ,lmax ,nstep
     character(len=maxlen) :: mask ,ncvm ,data ,oroot ,beam
     real(dp)              :: map_cf ,ncvm_cf
     logical               :: apply_pixwin

     type(real_ylm)                  :: y
     type(cosmo_params)              :: theta0
     class(cp_proposal) ,allocatable :: w
     real(dp) ,allocatable :: invN(:,:) ,AinvNA(:,:) ,AinvN(:,:) ,AinvSqrtN(:,:)
     real(dp) ,allocatable :: d(:) ,AinvNd(:)

   contains

     procedure :: init => init_gpar_data
     procedure :: read => read_params_from_file
     
  end type gpar_data
  
contains

  subroutine read_params_from_file(self,filename,verbose)
    class(gpar_data)   ,intent(out) :: self
    character(len=*)   ,intent(in)  :: filename
    logical ,optional  ,intent(in)  :: verbose
    
    type(paramfile_handle)  :: handle
    logical                 :: verb ,sample(ncp)
    real(dp)                :: tmpcov(ncp,ncp) ,par
    integer(i4b)            :: i ,unit ,s1 ,s2 ,s3 ,s4
    character(len=filenamelen) :: str
    
    if (present(verbose)) then
       verb = verbose
    else
       verb = .false.
    end if

    handle = parse_init(filename)
    self%ndata = parse_int(handle,'ndata',1)
    if (self%ndata .gt. 1) stop 'ndata > 1 not supported yet'

    self%nside = parse_int(handle,'nside')
    self%lmin  = parse_int(handle,'lmin')
    self%lmax  = parse_int(handle,'lmax')
    self%nstep = parse_int(handle,'nstep')
    self%lswitch = parse_int(handle,'lswitch')
    self%ncvm  = parse_string(handle,'ncvm')
    self%mask  = parse_string(handle,'mask')
    self%data  = parse_string(handle,'map')
    self%beam  = parse_string(handle,'beam')
    self%oroot = parse_string(handle,'output_root')
    self%ncvm_cf = parse_double(handle,'ncvm_cf')
    self%map_cf  = parse_double(handle,'map_cf')
    self%apply_pixwin = parse_lgt(handle,'apply_pixwin')

    s1 = parse_int(handle,'seed1')
    s2 = parse_int(handle,'seed2',0)
    s3 = parse_int(handle,'seed3',0)
    s4 = parse_int(handle,'seed4',0)

    call init_rng(s1 ,s2 ,s3 ,s4)

    call self%theta0%set_default()
    par = parse_double(handle,'ombh2')
    call self%theta0%set(ombh2=par)
    sample(1) = parse_lgt(handle,'sample_ombh2')

    par = parse_double(handle,'omch2')
    call self%theta0%set(omch2=par)
    sample(2) = parse_lgt(handle,'sample_omch2')

    par = parse_double(handle,'H0')
    call self%theta0%set(H0=par)
    sample(3) = parse_lgt(handle,'sample_H0')

    par = parse_double(handle,'tau')
    call self%theta0%set(tau=par)
    sample(4) = parse_lgt(handle,'sample_tau')

    par = parse_double(handle,'As')
    call self%theta0%set(As=par)
    sample(5) = parse_lgt(handle,'sample_As')

    par = parse_double(handle,'ns')
    call self%theta0%set(ns=par)
    sample(6) = parse_lgt(handle,'sample_ns')

    par = parse_double(handle,'r')
    call self%theta0%set(r=par)
    sample(7) = parse_lgt(handle,'sample_r')

    str = parse_string(handle,'proposal_type')
    select case(trim(str))
    case('gaussian')
       allocate(cp_gaussian_proposal :: self%w)
       str = parse_string(handle,'proposal_covmat')
       open(newunit=unit,file=str,status='old',action='read')
       do i = 1,ncp 
          read(unit,*) tmpcov(:,i)
       end do
    case default
       stop "proposal_type must be 'gaussian' "
    end select
    select type(w => self%w)
    type is (cp_gaussian_proposal)   
       call w%init_gaussian_proposal(sample,tmpcov)
    end select
    
    if (self%lmax .gt. 4*self%nside) then
       self%lmax  =  4*self%nside
       write(*,*) 'Healpix only supports modes up to 4Nside'
       write(*,*) 'Setting lmax = 4Nside'
    end if

    if (self%lswitch .gt. self%lmax) then
       self%lswitch  =  4*self%lmax
       write(*,*) 'Setting lswitch = lmax'
    end if
    
    if(verb) call parse_summarize(handle)
    call parse_finish(handle)
    
  end subroutine read_params_from_file

  subroutine init_gpar_data(self)
    class(gpar_data)  ,intent(inout) :: self
!    class(gpar_param) ,intent(in)    :: P

    integer(i4b)          :: info
    integer(i8b)          :: rcl ,nfull ,unit 
    real(dp) ,allocatable :: tmp1(:,:) ,bl(:,:) 
    character(len=80)     :: bheader(80)
    integer(i4b) :: k ,i

    associate(y => self%y )

      allocate(bl(0:4*self%nside,6),source=0._dp)
      call fits2cl(self%beam,bl(0:self%lmax,1:3),self%lmax,3,bheader)
      if(self%apply_pixwin) then
         allocate(tmp1(0:4*self%nside,3))
         call pixel_window(tmp1,self%nside)
         bl(:,1) = bl(:,1)*tmp1(:,1)
         bl(:,2) = bl(:,2)*tmp1(:,2)
         bl(:,3) = bl(:,3)*tmp1(:,3)
         deallocate(tmp1)
      end if

      call y%init(self%mask, self%lmin, self%lmax ,wl = bl)
      deallocate(bl)
      if(y%s%nside .ne. self%nside) stop 'mask has wrong nside'

      nfull = nside2npix(y%s%nside)*y%npol
      allocate(self%invN(nfull,nfull))
    
      inquire(iolength=rcl) self%invN
      open(newunit=unit,file=trim(self%ncvm),status='old',action='read',form='unformatted',access='direct',recl=rcl)
      read(unit,rec=1) self%invN
      close(unit)

      self%invN = self%invN*self%ncvm_cf

      call mask_matrix(y%lmask,self%invN)
      !N = L L^t
      call dpotrf('L',y%ntot,self%invN,y%ntot,info)
      if (info.ne.0) stop 'NCVM is not invertible'

      tmp1 = self%invN
      do i = 1,y%ntot
         do k = 1,i-1
            tmp1(k,i) = 0._dp
         end do
      end do
      !N^-1/2 = L^-1
      call dtrtri('L','N',y%ntot,tmp1,y%ntot,info)
      if (info.ne.0) stop 'sqrt NCVM is not invertible'
   
      bl = transpose(y%ylm)
      allocate(self%AinvSqrtN ,mold = bl)

      !A N^-1/2 = A L^-1 -> self%AinvSqrtN  
      call dgemm('N','N',y%nall,y%ntot,y%ntot,1.d0,bl,y%nall,tmp1,y%ntot,0.d0,self%AinvSqrtN,y%nall)
      call move_alloc(from=bl,to=tmp1)     

      !N^-1 = self%invN
      call dpotri('L',y%ntot,self%invN,y%ntot,info)
      do i = 1,y%ntot
         do k = i+1,y%ntot
            self%invN(i,k) = self%invN(k,i)
         end do
      end do

      allocate(self%AinvN ,mold = tmp1)
      
      !AN^-1 -> self%AinvN 
      call dgemm('N','N',y%nall,y%ntot,y%ntot,1.d0,tmp1,y%nall,self%invN,y%ntot,0.d0,self%AinvN,y%nall)
      deallocate(tmp1)

      !AN^-1A -> self%AinvNA
      allocate(self%AinvNA(y%nall,y%nall) ,source = 0._dp)
      call dgemm('N','N',y%nall,y%nall,y%ntot,1.d0,self%AinvN,y%nall,y%ylm,y%ntot,0.d0,self%AinvNA,y%nall)
!      call dgemm('N','T',y%nall,y%nall,y%ntot,1.d0,self%AinvN,y%nall,tmp1,y%nall,0.d0,self%AinvNA,y%nall)
   
      call read_map(self%data,tmp1,y%s)
      tmp1 = tmp1*self%map_cf
      self%d = map2vec(tmp1)
      
      !AN^-1 d -> self%AinvNd
      call mask_vec(y%lmask,self%d)
      allocate(self%AinvNd(y%nall) ,source = 0._dp)
      call dgemv('N',y%nall,y%ntot,1.d0,self%AinvN,y%nall,self%d,1,0.d0,self%AinvNd,1)

    end associate
            
  end subroutine init_gpar_data
  
end module gpar_data_mod
