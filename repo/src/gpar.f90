program gpar

  use cosmo_params_mod
  use chain_mod
  use gpar_data_mod 
  use healpix_types ,only : i4b
  implicit none

  type(chain_sample)     :: C
  type(gpar_data)        :: D
  character(filenamelen) :: parfile 
  integer(i4b)           :: ncmd ,i

  ncmd = command_argument_count()
  if(ncmd .ne. 1) then
     write(*,*) 'CRITICAL ERROR : usage gpar <params.ini>'
     stop
  else
     call get_command_argument(1,parfile)
  end if

  call D%read(parfile)
  call D%init()
  
  call C%Init(1,D%theta0,D%lmin,D%lmax,D%y%nall,D%nside)
  do i = 1,D%nstep
     call C%new_sample(D)
  end do
  
end program gpar
