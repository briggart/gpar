module CAMB_interface_mod
  
  use healpix_types ,only : dp
  use LambdaGeneral ,only : w_lam ,cs2_lam
  use camb ,only : cambparams ,CAMB_ValidateParams ,CAMB_GetResults ,lmin &
       ,lmax_lensed ,Cl_lensed ,Cl_tensor ,CT_Temp ,CT_Cross ,CT_E ,CT_B  &
       ,DoTensorNeutrinos ,DoLateRadTruncation ,NonLinear_none &
       ,HighAccuracyDefault ,camb_setdefparams ,AccuracyBoost &
       ,lAccuracyBoost
  use cls_mod
  use cosmo_params_mod ,only : cosmo_params

  implicit none

  private
!  public cosmo_params ,get_cls ,init_camb 
  public get_cls ,init_camb 

  type(cambparams) ,save :: CP 

  real(dp) ,parameter :: omnuh2_ref = 0.00064_dp
  real(dp) ,save      :: output_factor = 7.42835025e12_dp !defaul units uK

contains
  
  subroutine params2cambparams(P,CP)
    type(cosmo_params)  ,intent(in) :: P
    type(cambparams) ,intent(inout) :: CP

    real(dp)  :: hconv

    hconv = (P%H0/100._dp)**2

    CP%omegab = P%ombh2/hconv
    CP%omegac = P%omch2/hconv
    CP%omegan = omnuh2_ref/hconv
    CP%omegav = 1._dp -CP%omegab -CP%omegac -CP%omegan !flat universe for now  
    CP%H0     = P%H0

    CP%InitPower%an(1)             = P%ns
    CP%InitPower%ScalarPowerAmp(1) = P%As
    CP%InitPower%rat(1)            = P%r

    CP%Reion%optical_depth         = P%tau
  end subroutine params2cambparams

  subroutine get_cls(P,cl,ok)
    type(cosmo_params) ,intent(in) :: P
    type(aps)       ,intent(inout) :: cl
    logical ,optional ,intent(out) :: ok

    integer :: linf ,lsup ,lswitch ,error
    logical ,save :: firstrun = .true.

    if (present(ok)) ok = .true.
    if (firstrun) then
       call init_camb()
       firstrun = .false.
    end if

    call params2cambparams(P,CP)
    if (.not. CAMB_ValidateParams(CP)) then
       if (present(ok)) then
          ok = .false.
          return
       else
          stop "Parameters failed validation. Stopping"
       end if
    end if
    call CAMB_GetResults(CP,error=error)
    
    linf = max(lmin ,cl%lmin)
    lsup = min(lmax_lensed ,cl%lmax)     

    lswitch = min(CP%Max_l_tensor ,lsup)
    
    cl%cl(linf:lswitch,myTT) = output_factor*(Cl_lensed(linf:lswitch,1,CT_Temp) +Cl_tensor(linf:lswitch,1,CT_Temp))
    cl%cl(linf:lswitch,myTE) = output_factor*(Cl_lensed(linf:lswitch,1,CT_Cross) +Cl_tensor(linf:lswitch,1,CT_Cross))
    cl%cl(linf:lswitch,myEE) = output_factor*(Cl_lensed(linf:lswitch,1,CT_E) +Cl_tensor(linf:lswitch,1,CT_E))
    cl%cl(linf:lswitch,myBB) = output_factor*(Cl_lensed(linf:lswitch,1,CT_B) +Cl_tensor(linf:lswitch,1,CT_B))

    cl%cl(lswitch+1:lsup,myTT) = output_factor*Cl_lensed(lswitch+1:lsup,1,CT_Temp)
    cl%cl(lswitch+1:lsup,myTE) = output_factor*Cl_lensed(lswitch+1:lsup,1,CT_Cross)
    cl%cl(lswitch+1:lsup,myEE) = output_factor*Cl_lensed(lswitch+1:lsup,1,CT_E)
    cl%cl(lswitch+1:lsup,myBB) = output_factor*Cl_lensed(lswitch+1:lsup,1,CT_B)

  end subroutine get_cls

  subroutine init_camb(parfile)
    !parfile not supported for now
    character(len=*) ,intent(in) ,optional :: parfile

    if (present(parfile)) print*,'parfile not supported for now'

    !init CAMB Params to default values
    call CAMB_SetDefParams(CP) 
    
    !now set our defaults
    CP%WantCls = .true.
    CP%WantScalars = .true.
    CP%WantVectors = .false.
    CP%WantTensors = .true.
    CP%DoLensing   = .true.

    CP%Max_l        = 1200 
    CP%Max_eta_k    = 2*CP%Max_l
    CP%Max_l_tensor = 600
    CP%Max_eta_k_tensor = 2*CP%Max_l_tensor
    
    

    !PK transfer function
    CP%PK_WantTransfer = .false.
    CP%Transfer%Pk_num_redshifts = 1
    CP%Transfer%Pk_redshifts     = 0

    CP%tcmb = 2.7255_dp
    CP%yhe  = 0.24_dp

    !neutrinos
    CP%num_nu_massless     = 2.046_dp
    CP%nu_mass_eigenstates = 1
    CP%omegan              = omnuh2_ref/(CP%H0/100)**2
    CP%num_nu_massive      = 1 
    CP%share_delta_neff    = .true.
    CP%nu_mass_fractions(1)= 1
    DoTensorNeutrinos      = .true.
    DoLateRadTruncation    = .true.

    !only linear Pk
    CP%NonLinear  = NonLinear_none

    CP%AccuratePolarization = .true.
    CP%AccurateReionization = .true.
    CP%AccurateBB           = .true.

    HighAccuracyDefault     = .true.
    AccuracyBoost           = 3
    lAccuracyBoost        = 3

    !initial power stuff  (see power_tilt.f90)
    associate(IP => CP%InitPower)
       IP%k_0_scalar        = 0.05
       IP%k_0_tensor        = 0.05
       IP%nn                = 1
       IP%an(1)             = 0.96
       IP%n_run(:)          = 0.d0
       IP%n_runrun(:)       = 0.d0
       IP%ant(1)            = 0.d0
       IP%nt_run(:)         = 0.d0
       IP%ScalarPowerAmp(1) = 2.1d-9
       IP%rat(1)            = 1
       IP%tensor_parameterization = 1
    end associate

    !reionization (see reionization.f90)
    associate(R => CP%Reion)
       R%use_optical_Depth     = .true.
       R%optical_depth         = 0.06_dp
       R%delta_redshift        = 0.5_dp
       R%helium_redshift       = 3.5_dp
       R%helium_delta_redshift = 0.5_dp
!       R%fraction              = Reionization_DefFraction
       R%fraction              = -1._dp
       R%helium_redshiftstart  = R%helium_redshift +3_dp*R%helium_delta_redshift
    end associate   

    !dark energy (see equations.f90) 
    w_lam   = -1._dp
!    wa_ppf  =  0._dp
    cs2_lam =  1._dp

    !recombination (see recfast.f90, if using a different recombination module will need to update defaults)
    associate(R => CP%recomb)
       R%RECFAST_fudge    = 1.14_dp
       R%RECFAST_fudge_He = 0.86_dp
       R%RECFAST_Heswitch = 6
       R%RECFAST_Hswitch  = .true.
    end associate

    !dummy run to set up everything internally
    call CAMB_GetResults(CP)

  end subroutine init_camb

end module camb_interface_mod
