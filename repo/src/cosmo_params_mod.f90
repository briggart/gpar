module cosmo_params_mod

  use Healpix_types ,only : dp
  use lpl_utils_mod ,only : mask_matrix
  use rng_mod
  implicit none

  integer ,parameter :: ncp = 7

  public cosmo_params ,ncp ,cp_gaussian_proposal

  !basic parameter set, to be extended later                                  
  type :: cosmo_params
 
     !physical barion density     
     real(dp) :: ombh2 
     !physical CDM density     
     real(dp) :: omch2 
     !Hubble parameter (in units of K/s MPc^-1)     
     real(dp) :: H0  
     !reionization optical depth     
     real(dp) :: tau 
     !primordial power spectrum amplitude     
     real(dp) :: As  
     !primordial power spectrum spectral index     
     real(dp) :: ns  
     !tensor-to-scalar ratio
     real(dp) :: r   
     
   contains

     !probably some stuff to initialize values
     procedure :: init => init_cosmo_params
     procedure :: set => set_cosmo_params
     procedure :: set_default => set_default_params
     procedure :: par2vec => params2vector
     procedure :: vec2par => vector2params
     final     :: final_cpar
     
  end type cosmo_params

  type ,abstract :: cp_proposal

     logical :: sample(ncp)
     integer :: nsample
     
   contains

     procedure(propose_cp) ,deferred :: propose 
     procedure ,nopass               :: init_propose_cp
     generic                         :: init => init_propose_cp
     
  end type cp_proposal

  
  abstract interface
     subroutine propose_cp(self,oldpar,newpar)
       import cp_proposal ,cosmo_params 
       class(cp_proposal)  ,intent(inout) :: self
       class(cosmo_params) ,intent(in)    :: oldpar
       class(cosmo_params) ,intent(out)   :: newpar
     end subroutine propose_cp
  end interface
  
  type ,extends(cp_proposal) :: cp_gaussian_proposal
     
     real(dp) ,allocatable :: cov(: ,:)

   contains

     procedure :: init_gaussian_proposal
     procedure :: propose => cp_propose_gaussian
     generic   :: init => init_gaussian_proposal 
     
  end type cp_gaussian_proposal

contains

  subroutine final_cpar(self)
    type(cosmo_params) ,intent(inout) :: self
    
  end subroutine final_cpar
  
  subroutine init_propose_cp()
    !empty, children will need to implement their onw initialization procedure
  end subroutine init_propose_cp

  subroutine init_cosmo_params(self)
    class(cosmo_params) ,intent(inout) :: self
    
  end subroutine init_cosmo_params
  
  subroutine set_cosmo_params(self,ombh2,omch2,H0,tau,as,ns,r)
    class(cosmo_params) ,intent(inout) :: self
    real(dp) ,optional  ,intent(in)    :: ombh2,omch2,H0,tau,as,ns,r

!    if(.not. associated(self%p)) call self%init()
     
    if(present(ombh2)) then
       self%ombh2 = ombh2
    end if
    if(present(omch2)) then
       self%omch2 = omch2
    end if
    if(present(H0)) then
       self%H0 = H0
    end if
    if(present(tau)) then
       self%tau = tau
    end if
    if(present(as)) then
       self%as = as
    end if
    if(present(ns)) then
       self%ns = ns
    end if
    if(present(r)) then
       self%r = r
    end if
    
  end subroutine set_cosmo_params

  function params2vector(self) result(p)
    class(cosmo_params) ,intent(in) :: self
    real(dp)                        :: p(ncp)

    p(1) = self%ombh2
    p(2) = self%omch2
    p(3) = self%H0
    p(4) = self%tau
    p(5) = self%ns
    p(6) = self%As
    p(7) = self%r

  end function params2vector

  subroutine vector2params(self,p) 
    class(cosmo_params) ,intent(inout) :: self
    real(dp)            ,intent(in)    :: p(ncp)

    self%ombh2 = p(1)
    self%omch2 = p(2)
    self%H0    = p(3)
    self%tau   = p(4)
    self%ns    = p(5)
    self%As    = p(6)
    self%r     = p(7)

  end subroutine vector2params
  
  subroutine set_default_params(self)
    !set params to Planck 2018 defaults TT,TE,EE+lowE+lensing
    !mostly for testing 
    class(cosmo_params) ,intent(inout) :: self

    call self%set(ombh2 = 0.022383_dp &
         ,omch2 = 0.12011_dp &
         ,H0    = 67.32_dp &
         ,ns    = 0.96605_dp &
         ,r     = 0._dp &
         ,tau   = 0.0543_dp &
         ,As    = 2.10058e-9_dp )
    
  end subroutine set_default_params
   
  subroutine cp_propose_gaussian(self,oldpar,newpar)

    class(cp_gaussian_proposal) ,intent(inout) :: self
    class(cosmo_params)         ,intent(in)    :: oldpar
    class(cosmo_params)         ,intent(out)   :: newpar

    real(dp) :: p(self%nsample)
    integer  :: i

    call newpar%init()

    do i = 1,self%nsample
       p(i) = rand_g()
    end do

    call dtrmv('L','N','N',self%nsample,self%cov,self%nsample,p,self%nsample)
    call newpar%vec2par(unpack(p,mask=self%sample,field= 0._dp) +oldpar%par2vec())
    
  end subroutine cp_propose_gaussian

  subroutine init_gaussian_proposal(self,sample,cov)

    class(cp_gaussian_proposal) ,intent(inout) :: self
    logical                     ,intent(in)    :: sample(ncp)
    real(dp)                    ,intent(in)    :: cov(:,:)

    integer :: n ,info

    self%sample  = sample
    self%nsample = count(sample)
    
    n = size(cov(:,1))
    if ( n .ne. ncp) stop 'proposal covariance has wrong # of rows'
    n = size(cov(1,:))
    if ( n .ne. ncp) stop 'proposal covariance has wrong # of columns'

    self%cov = cov
    call mask_matrix(sample,self%cov)
    call dpotrf('L',self%nsample,self%cov,self%nsample,info)
    if (info .ne. 0) stop 'proposal covariance is not invertible'
  
  end subroutine init_gaussian_proposal
  
end module cosmo_params_mod
